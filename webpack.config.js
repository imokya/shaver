const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')


module.exports = {
  entry: {
    app: resolve('./src/app.js')
  },
  output: {
    path: resolve('./dist'),
    filename: '[name].js',
    publicPath: '/'
  },
  resolve: {
    alias: {
      assets: resolve('./src/assets')
    }
  },
  devtool: 'source-map',
  devServer: {
    stats: 'minimal'
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:6].[ext]'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new FriendlyErrorsWebpackPlugin()
  ]
}

function resolve(_path) {
  return path.join(__dirname, _path)
}