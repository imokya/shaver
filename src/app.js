import './styles/style.css'

const touch = !!('ontouchstart' in window)
const event = {
  touchBegan: touch ? 'touchstart' : 'mousedown',
  touchMoved: touch ? 'touchmove' : 'mousemove',
  touchEnded: touch ? 'touchend' : 'mouseup'
}
const brushRadius = 20
let points = []

const app = {

  init() {
    this.clearEl = document.querySelector('.reset')
    this.infoEl = document.querySelector('.percent')
    this.canvas = document.querySelector('#canvas')
    this.ctx = canvas.getContext('2d')
    this.bindEvents()
    this.draw()
  },

  bindEvents() {
    this.canvas.addEventListener(event.touchBegan, this.touchBegan.bind(this))
    this.canvas.addEventListener(event.touchMoved, this.touchMoved.bind(this))
    this.canvas.addEventListener(event.touchEnded, this.touchEnded.bind(this))
    this.clearEl.addEventListener('click', this.reset.bind(this))
  },

  reset() {
    points = []
    this.ctx.globalCompositeOperation = 'source-over'
    this.infoEl.innerText = '0%'
    this.draw()
  },

  touchBegan(e) {
    this.active = true
    this.ctx.save()
    this.ctx.fillStyle = '#fff'
    this.ctx.globalCompositeOperation = 'destination-out'
  },

  touchMoved(e) {
    if (this.active) {
      let x = touch ? e.changedTouches[0].pageX : e.pageX
      let y = touch ? e.changedTouches[0].pageY : e.pageY
      x -= this.canvas.offsetLeft
      y -= this.canvas.offsetTop
      this.checkProgress(x, y)
      this.ctx.beginPath()
      this.ctx.arc(x, y, brushRadius, 0, Math.PI * 2)
      this.ctx.fill()
    }
  },

  touchEnded(e) {
    this.active = false
    this.ctx.restore()
  },

  checkProgress(x, y) {
  
    points.forEach((point, index)=> {
      const dx = point.x - x
      const dy = point.y - y
      if ((dx*dx + dy*dy) < brushRadius * brushRadius) {
        points.splice(index, 1)
        const percent = (this.totalPoints - points.length) / this.totalPoints * 100 | 0
        this.infoEl.innerText = percent + '%'
      }
    })
  },

  getData() {
    const w = this.canvas.width
    const h = this.canvas.height
    const data = this.ctx.getImageData(0, 0, w, h).data
    for (let i = 0; i < data.length / 4; i += brushRadius) {
      if (data[i*4+3] > 0) {
        points.push({
         x: i % w,
         y: i / w | 0
        })
      }
    }
    this.totalPoints = points.length
  },

  draw() {
    const img = new Image()
    img.onload = () => {
      this.ctx.drawImage(img, 0, 0)
      this.getData()
    }
    img.src = require('assets/avatar-bear.png')
  }

}

app.init()